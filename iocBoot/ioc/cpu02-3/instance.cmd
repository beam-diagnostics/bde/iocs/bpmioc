###############################################################################
#- Macro line should start with '### MACRO' folowed by macro name
#- and default value:
#-   ### MACRO <MACRO NAME> <MACRO DEFAULT VALUE>
#-
#- All the lines (except ### MACRO lines) will be copied verbatim to final
#- st.cmd file in the instance folder.
#-
#- Following macros need to be defined here as they are expected in the
#- common.cmd.
epicsEnvSet("CONTROL_GROUP", "LAB-BPM02")
epicsEnvSet("AMC_NAME", "Ctrl-AMC-003")
epicsEnvSet("AMC_DEVICE", "/dev/sis8300-3")
epicsEnvSet("EVR_NAME", "LAB-BPM02:TS-EVR-000:")

epicsEnvSet("SYSTEM1_PREFIX", "LAB-002:PBI-BPM-003:")
epicsEnvSet("SYSTEM1_NAME", "Lab BPM 2#3")

epicsEnvSet("SYSTEM2_PREFIX", "LAB-002:PBI-BPM-004:")
epicsEnvSet("SYSTEM2_NAME", "Lab BPM 2#4")
###############################################################################
