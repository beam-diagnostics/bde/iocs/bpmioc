###############################################################################
#- Macro line should start with '### MACRO' folowed by macro name
#- and default value:
#-   ### MACRO <MACRO NAME> <MACRO DEFAULT VALUE>
#-
#- All the lines (except ### MACRO lines) will be copied verbatim to final
#- st.cmd file in the instance folder.
#-
#- Following macros need to be defined here as they are expected in the
#- common.cmd.
epicsEnvSet("CONTROL_GROUP", "LAB-BPM02")
epicsEnvSet("AMC_NAME", "Ctrl-AMC-002")
epicsEnvSet("AMC_DEVICE", "/dev/sis8300-2")
epicsEnvSet("EVR_NAME", "LAB-BPM02:TS-EVR-000:")

epicsEnvSet("SYSTEM1_PREFIX", "LAB-002:PBI-BPM-001:")
epicsEnvSet("SYSTEM1_NAME", "Lab BPM 2#1")

epicsEnvSet("SYSTEM2_PREFIX", "LAB-002:PBI-BPM-002:")
epicsEnvSet("SYSTEM2_NAME", "Lab BPM 2#2")
###############################################################################
