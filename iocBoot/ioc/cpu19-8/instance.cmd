###############################################################################
#- Macro line should start with '### MACRO' folowed by macro name
#- and default value:
#-   ### MACRO <MACRO NAME> <MACRO DEFAULT VALUE>
#-
#- All the lines (except ### MACRO lines) will be copied verbatim to final
#- st.cmd file in the instance folder.
#-
#- Following macros need to be defined here as they are expected in the
#- common.cmd.
epicsEnvSet("CONTROL_GROUP", "LAB-BPM19")
epicsEnvSet("AMC_NAME", "Ctrl-AMC-008")
epicsEnvSet("AMC_DEVICE", "/dev/sis8300-8")
epicsEnvSet("EVR_NAME", "LAB-BPM19:TS-EVR-000:")

epicsEnvSet("SYSTEM1_PREFIX", "LAB-019:PBI-BPM-011:")
epicsEnvSet("SYSTEM1_NAME", "Lab BPM 19#11")

epicsEnvSet("SYSTEM2_PREFIX", "LAB-019:PBI-BPM-012:")
epicsEnvSet("SYSTEM2_NAME", "Lab BPM 19#12")
###############################################################################
