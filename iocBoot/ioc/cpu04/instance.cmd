###############################################################################
#- Macro line should start with '### MACRO' folowed by macro name
#- and default value:
#-   ### MACRO <MACRO NAME> <MACRO DEFAULT VALUE>
#-
#- All the lines (except ### MACRO lines) will be copied verbatim to final
#- st.cmd file in the instance folder.
#-
#- Following macros need to be defined here as they are expected in the
#- common.cmd.
epicsEnvSet("CONTROL_GROUP", "LAB-BPM04")
epicsEnvSet("AMC_NAME", "Ctrl-AMC-004")
epicsEnvSet("AMC_DEVICE", "/dev/sis8300-4")
epicsEnvSet("EVR_NAME", "LAB-BPM04:TS-EVR-000:")

epicsEnvSet("SYSTEM1_PREFIX", "LAB-004:PBI-BPM-001:")
epicsEnvSet("SYSTEM1_NAME", "Lab BPM 4#1")

epicsEnvSet("SYSTEM2_PREFIX", "LAB-004:PBI-BPM-002:")
epicsEnvSet("SYSTEM2_NAME", "Lab BPM 4#2")
###############################################################################
