#!/bin/bash

export PATH=/data/bdee/devel/bpm_next/stage/bin:$PATH

ALLSLOTS="3 4 6 7"
SLOTS="$@"
[[ -n $SLOTS ]] || { echo usage: $0 slots; exit 1; }

# stop all
for i in $ALLSLOTS; do
  caput LAB:AMC-002-$i:Acquire 0
done

sleep 1

# enable only selected slots
for i in $SLOTS; do
  caput LAB:AMC-002-$i:Acquire 1
done
