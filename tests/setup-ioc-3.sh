#!/bin/bash

export PATH=/data/bdee/devel/bpm_next/stage/bin:$PATH

ALLSLOTS="3 4 6 7"

# check the rate and pulse counts
for i in $ALLSLOTS; do
  caget LAB:AMC-002-$i:ArrayRate_RBV
  caget LAB:AMC-002-$i:PulseCountR
done
