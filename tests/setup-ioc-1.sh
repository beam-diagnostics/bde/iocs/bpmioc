#!/bin/bash

export PATH=/data/bdee/devel/bpm_devel/stage/bin:$PATH

ALLSLOTS="2 3 4 5 6 7 9 10"
CPUNR=$1
AMCNR=0$1
NRSAMPLES=$2
[[ -n $AMCNR ]] || { echo usage: $0 amcnr nrsamples; exit 1; }
[[ -n $NRSAMPLES ]] || { echo usage: $0 amcnr nrsamples; exit 1; }

# stop all
for i in $ALLSLOTS; do
  caput LAB:AMC-$AMCNR-$i:Acquire 0
done

sleep 1

# set sample count, start all
for i in $ALLSLOTS; do
  caput LAB:AMC-$AMCNR-$i:NumSamples $NRSAMPLES
  caput LAB:AMC-$AMCNR-$i:TriggerRepeat -1
  caput LAB:AMC-$AMCNR-$i:Acquire 1
done

sleep 2

# show rate, should be 14 == acq running
for i in $ALLSLOTS; do
  caget LAB:AMC-$AMCNR-$i:ArrayRate_RBV
done


# make a date stamp in the IOC shells to easily spot new error messages if any
for i in $ALLSLOTS; do
  PORT=$((2000+$i))
  ssh dev@bd-cpu$CPUNR \
  "(echo -e \"\r\n# -------------------------------------------------------------------------\"; \
   sleep 1; \
   echo -e \"date\r\n\"; \
   sleep 1; \
   echo -e \"\r\n\r\n\"; ) | telnet localhost $PORT"
done
